#ifndef PESSOA_HPP 
#define PESSOA_HPP 
#include<string>

using namespace std;

class Pessoa{

private:
	//Atributos
	string nome;
	string matricula;
	int idade;
	string sexo;
	string telefone;

public:	
	//Métodos
	Pessoa();//Método construtor (mesmo nome das classes)
	Pessoa(string nome,string matricula,string sexo,string telefone, int idade);
	~Pessoa();//Método destrutor (colocar o tio na frente do mesmo nome do construtor)
	string getNome();//Métodor acessor do tipo Get
	void setNome(string nome);//Acessor Set
	string getMatricula();
	void setMatricula(string matricula);
	int getIdade();
	void setIdade(int idade);
	string getSexo();
	void setSexo(string sexo);
	string getTelefone();
	void setTelefone(string telefone);
};

#endif
