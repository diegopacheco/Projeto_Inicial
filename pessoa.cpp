#include"pessoa.hpp"
#include<iostream>

using namespace std;

Pessoa::Pessoa(){
	nome="";
	matricula = "";
	idade = 0;
	telefone = "";
	sexo = "";
}

Pessoa::Pessoa(string nome,string telefone,string sexo,string matricula, int idade){
	setNome(nome);//this->nome = nome; (outra forma de fazer)
	setTelefone(telefone);//this->telefone = telefone;
	setIdade(idade);//this->idade = idade;
	setMatricula(matricula);
	setSexo(sexo);
}

Pessoa::~Pessoa(){}

string Pessoa::getNome(){
	return nome;
}

void Pessoa::setNome(string nome){
	this->nome = nome;
}

string Pessoa::getMatricula(){
	return matricula;
}

void Pessoa::setMatricula(string matricula){
	this -> matricula = matricula;
}

int Pessoa::getIdade(){
	return idade;
}

void Pessoa::setIdade(int idade){
	this->idade = idade;
}

string Pessoa::getSexo(){
	return sexo;
}

void Pessoa:: setSexo(string sexo)
{
	this -> sexo = sexo; 
}
string Pessoa::getTelefone(){
	return telefone;
}
void Pessoa::setTelefone(string telefone){
	this -> telefone = telefone;
}

